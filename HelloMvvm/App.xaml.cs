﻿using HelloMvvm.View;
using HelloMvvm.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace HelloMvvm
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            // HACK: add VMLocator resource here to get around race condition of 
            // who will get initialized first (XAML vs code-behind)
            App.Current.Resources.Add("Locator", new ViewModelLocator());

            this.Startup += this.Application_Startup;
            this.Exit += this.Application_Exit;
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var window = new MainWindow();
            window.Show();
        }

        private void Application_Exit(object sender, EventArgs e)
        {

        }

    }
}
