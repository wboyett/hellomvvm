﻿using HelloMvvm.Common;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloMvvm.Model
{
    public class PageNav : ObservableObject
    {
        public PageNav(object page, object selectedItem)
        {
            Page = page;
            SelectedItem = selectedItem;
        }

        public object Page
        {
            get; set;
        }

        public object SelectedItem
        {
            get; set;
        }

        public void CreateFakeData()
        {
            Page = DataUtils.RandomInt(3);
            SelectedItem = DataUtils.RandomInt(10);
        }
    }

}
