﻿using HelloMvvm.Common;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloMvvm.Model
{
    public class User : ObservableObject
    {
        public User()
        {

        }

        private int _UserId;
        public int UserId
        {
            get { return _UserId; }
            set { Set(ref _UserId, value, nameof(UserId)); }
        }

        private string _FirstName;
        public string FirstName
        {
            get { return _FirstName; }
            set { Set(ref _FirstName, value, nameof(FirstName)); }
        }

        private string _LastName;
        public string LastName
        {
            get { return _LastName; }
            set { Set(ref _LastName, value, nameof(LastName)); }
        }

        private string _Company;
        public string Company
        {
            get { return _Company; }
            set { Set(ref _Company, value, nameof(Company)); }
        }

        private string _Email;
        public string Email
        {
            get { return _Email; }
            set { Set(ref _Email, value, nameof(Email)); }
        }

        private string _Phone;
        public string Phone
        {
            get { return _Phone; }
            set { Set(ref _Phone, value, nameof(Phone)); }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set { Set(ref _Description, value, nameof(Description)); }
        }


        private DateTime _Dob;
        public DateTime Dob
        {
            get { return _Dob; }
            set { Set(ref _Dob, value, nameof(Dob)); }
        }

        private DateTime _CreatedOn;
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { Set(ref _CreatedOn, value, nameof(CreatedOn)); }
        }

        private DateTime _UpdatedOn;
        public DateTime UpdatedOn
        {
            get { return _UpdatedOn; }
            set { Set(ref _UpdatedOn, value, nameof(UpdatedOn)); }
        }

        
        public void CreateFakeData()
        {
            UserId = DataUtils.RandomInt();
            FirstName = DataUtils.RandomWords(1);
            LastName = DataUtils.RandomWords(1);
            Company = DataUtils.RandomWords(5);
            Email = DataUtils.RandomWords(1) + "@" + DataUtils.RandomWords(1) + ".com";
            Phone = DataUtils.RandomInt(111111111, 999999999).ToString();

            Dob = DataUtils.RandomDateTime(DateTime.Now - TimeSpan.FromDays(365 * 130), DateTime.Now);
            CreatedOn = DataUtils.RandomDateTime(DateTime.Now - TimeSpan.FromDays(365 * 10), DateTime.Now);
            UpdatedOn = DataUtils.RandomDateTime(CreatedOn, DateTime.Now);
        }
    }
}
