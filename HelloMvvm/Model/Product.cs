﻿using HelloMvvm.Common;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloMvvm.Model
{
    public class Product : ObservableObject
    {
        public Product()
        {

        }

        private int _ProductId;
        public int ProductId
        {
            get { return _ProductId; }
            set { Set(ref _ProductId, value, nameof(ProductId)); }
        }

        private string _ProductName;
        public string ProductName
        {
            get { return _ProductName; }
            set { Set(ref _ProductName, value, nameof(ProductName)); }
        }

        private string _ModelNumber;
        public string ModelNumber
        {
            get { return _ModelNumber; }
            set { Set(ref _ModelNumber, value, nameof(ModelNumber)); }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set { Set(ref _Description, value, nameof(Description)); }
        }

        private Dimensions _Dimensions;
        public Dimensions Dimensions
        {
            get { return _Dimensions; }
            set { Set(ref _Dimensions, value, nameof(Dimensions)); }
        }

        private double _Weight;
        public double Weight
        {
            get { return _Weight; }
            set { Set(ref _Weight, value, nameof(Weight)); }
        }



        private DateTime _CreatedOn;
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { Set(ref _CreatedOn, value, nameof(CreatedOn)); }
        }

        private DateTime _UpdatedOn;
        public DateTime UpdatedOn
        {
            get { return _UpdatedOn; }
            set { Set(ref _UpdatedOn, value, nameof(UpdatedOn)); }
        }


        public void CreateFakeData()
        {
            ProductId = DataUtils.RandomInt();
            ProductName = DataUtils.RandomWords(3);
            ModelNumber = DataUtils.RandomString(6,6);
            Description = DataUtils.RandomWords(100);
            Dimensions = new Dimensions(DataUtils.RandomFloat(0.1f, 30), DataUtils.RandomFloat(0.5f, 20), DataUtils.RandomFloat(1, 10));
            Weight = DataUtils.RandomFloat(0, 100);

            CreatedOn = DataUtils.RandomDateTime(DateTime.Now - TimeSpan.FromDays(365 * 10), DateTime.Now);
            UpdatedOn = DataUtils.RandomDateTime(CreatedOn, DateTime.Now);
        }
    }

    public class Dimensions : ObservableObject
    {
        public Dimensions(double length, double width, double height)
        {
            Length = length;
            Width = width;
            Height = height;
        }

        private double _Length;
        public double Length
        {
            get { return _Length; }
            set { Set(ref _Length, value, nameof(Length)); }
        }

        private double _Width;
        public double Width
        {
            get { return _Width; }
            set { Set(ref _Width, value, nameof(Width)); }
        }

        private double _Height;
        public double Height
        {
            get { return _Height; }
            set { Set(ref _Height, value, nameof(Height)); }
        }
    }
}
