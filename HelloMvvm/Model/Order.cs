﻿using HelloMvvm.Common;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloMvvm.Model
{
    public class Order : ObservableObject
    {
        public Order()
        {

        }

        private long _OrderId;
        public long OrderId
        {
            get { return _OrderId; }
            set { Set(ref _OrderId, value, nameof(OrderId)); }
        }

        private int _UserId;
        public int UserId
        {
            get { return _UserId; }
            set { Set(ref _UserId, value, nameof(UserId)); }
        }

        private int _ProductId;
        public int ProductId
        {
            get { return _ProductId; }
            set { Set(ref _ProductId, value, nameof(ProductId)); }
        }


        private string _OrderTitle;
        public string OrderTitle
        {
            get { return _OrderTitle; }
            set { Set(ref _OrderTitle, value, nameof(OrderTitle)); }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set { Set(ref _Description, value, nameof(Description)); }
        }


        private DateTime _CreatedOn;
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { Set(ref _CreatedOn, value, nameof(CreatedOn)); }
        }

        private DateTime _UpdatedOn;
        public DateTime UpdatedOn
        {
            get { return _UpdatedOn; }
            set { Set(ref _UpdatedOn, value, nameof(UpdatedOn)); }
        }

        private DateTime _OrderedOn;
        public DateTime OrderedOn
        {
            get { return _OrderedOn; }
            set { Set(ref _OrderedOn, value, nameof(OrderedOn)); }
        }


        public void CreateFakeData()
        {
            OrderId = DataUtils.RandomInt();
            ProductId = DataUtils.RandomInt();
            UserId = DataUtils.RandomInt();
            OrderTitle = DataUtils.RandomWords(10);
            Description = DataUtils.RandomWords(100);

            CreatedOn = DataUtils.RandomDateTime(DateTime.Now - TimeSpan.FromDays(365 * 10), DateTime.Now);
            UpdatedOn = DataUtils.RandomDateTime(CreatedOn, DateTime.Now);
            OrderedOn = DataUtils.RandomDateTime(UpdatedOn, DateTime.Now);
        }
    }

}
