﻿using HelloMvvm.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloMvvm.Service
{
    public enum Page
    {
        Users,
        Products,
        Orders,
        Reports
    }

    public interface INavigationService
    {
        ObservableCollection<PageNav> GetBackList();
        ObservableCollection<PageNav> GetForwardList();

        void GotoUserPage(User page);
        void GotoProductPage(Product product);
        void GotoOrderPage(Order order);
        void GotoReportPage();
    }
}
