﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloMvvm.Model;
using GalaSoft.MvvmLight.Messaging;
using HelloMvvm.ViewModel;

namespace HelloMvvm.Service
{
    abstract class NavigationServiceBase : INavigationService
    {
        public NavigationServiceBase()
        {
            HistoryList = new ObservableCollection<Order>();
            BackList = new ObservableCollection<Order>();
            ForwardList = new ObservableCollection<Order>();
        }

        #region Keep track of what pages have been visited (and what items)

        protected int HistoryIndex;
        protected ObservableCollection<Order> HistoryList;

        protected ObservableCollection<Order> BackList;
        public ObservableCollection<PageNav> GetBackList()
        {
            throw new NotImplementedException();
        }

        protected ObservableCollection<Order> ForwardList;
        public ObservableCollection<PageNav> GetForwardList()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Goto specific page (and specific item)

        private void SetCurrentPage(PageNav page)
        {
            Messenger.Default.Send<PageNav, MainViewModel>(page);
        }

        public void GotoUserPage(User user)
        {
            SetCurrentPage(new PageNav(Page.Users, user));
        }

        public void GotoProductPage(Product product)
        {
            SetCurrentPage(new PageNav(Page.Products, product));
        }

        public void GotoOrderPage(Order order)
        {
            SetCurrentPage(new PageNav(Page.Orders, order));
        }

        public void GotoReportPage()
        {
            SetCurrentPage(new PageNav(Page.Reports, null));
        }

        #endregion
    }
}
