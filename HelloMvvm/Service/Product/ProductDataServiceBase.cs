﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloMvvm.Model;

namespace HelloMvvm.Service
{
    class ProductDataServiceBase : IProductDataService
    {
        public ProductDataServiceBase()
        {
            ProductList = new ObservableCollection<Product>();
        }

        protected ObservableCollection<Product> ProductList;
        public ObservableCollection<Product> GetProductList()
        {
            return ProductList;
        }

    }
}
