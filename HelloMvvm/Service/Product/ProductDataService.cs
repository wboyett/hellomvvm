﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloMvvm.Model;

namespace HelloMvvm.Service
{
    class ProductDataService : ProductDataServiceBase
    {
        public ProductDataService()
        {
            FakeDatabase.Products.ForEach(ProductList.Add);
        }


        public void AddNewProduct(Product prod)
        {
            ProductList.Add(prod);
        }
    }
}
