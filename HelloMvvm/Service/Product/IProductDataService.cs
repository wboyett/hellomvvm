﻿using HelloMvvm.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloMvvm.Service
{
    public interface IProductDataService
    {
        ObservableCollection<Product> GetProductList();
    }
}
