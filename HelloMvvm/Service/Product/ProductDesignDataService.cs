﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloMvvm.Model;

namespace HelloMvvm.Service
{
    class ProductDesignDataService : ProductDataServiceBase
    {
        public ProductDesignDataService()
        {
            for (int i = 0; i < 10; i++)
            {
                var product = new Product();
                product.CreateFakeData();
                ProductList.Add(product);
            }
        }

    }
}
