﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloMvvm.Model;
using System.Threading;

namespace HelloMvvm.Service
{
    class UserDataService : UserDataServiceBase
    {
        
        public UserDataService()
        {
            FakeDatabase.Users.ForEach(UserList.Add);
        }

        public void AddNewUser(User user)
        {
            UserList.Add(user);
        }
    }
}
