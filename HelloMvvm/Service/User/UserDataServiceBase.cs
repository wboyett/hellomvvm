﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloMvvm.Model;

namespace HelloMvvm.Service
{
    class UserDataServiceBase : IUserDataService
    {
        public UserDataServiceBase()
        {
            UserList = new ObservableCollection<User>();
        }

        protected ObservableCollection<User> UserList;
        public ObservableCollection<User> GetUserList()
        {
            return UserList;
        }

    }
}
