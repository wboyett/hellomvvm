﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloMvvm.Model;

namespace HelloMvvm.Service
{
    class UserDesignDataService : UserDataServiceBase
    {
        public UserDesignDataService()
        {
            for (int i = 0; i < 10; i++)
            {
                var user = new User();
                user.CreateFakeData();
                UserList.Add(user);
            }
        }

    }
}
