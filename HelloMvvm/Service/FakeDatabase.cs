﻿using HelloMvvm.Common;
using HelloMvvm.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HelloMvvm.Service
{
    class FakeDatabase
    {
        public int NUM_USERS = 50;
        public int NUM_PRODUCTS = 10;
        public int NUM_ORDERS = 500;

        public FakeDatabase()
        {
            InitializeUserList();
            UserUpdateTimer = new Timer(AddUser, null, 1000, 10000);

            InitializeProductList();
            //ProductUpdateTimer = new Timer(AddUser, null, 1000, 100000);

            InitializeOrderList();
            OrderUpdateTimer = new Timer(AddOrder, null, 1000, 1000);

        }



        public static List<User> Users = new List<User>();
        private Timer UserUpdateTimer;
        private static int UserCount;
        private void InitializeUserList()
        {
            UserCount = 0;
            for (int i = 0; i < NUM_USERS; i++)
            {
                AddUser(null);
            }
        }
        private void AddUser(object state)
        {
            App.Current.Dispatcher.Invoke(() =>
            {
                var user = new User();
                user.CreateFakeData();
                user.UserId = UserCount + 1;
                Users.Add(user);
                UserCount++;
            });
        }


        public static List<Product> Products = new List<Product>();
        private Timer ProductUpdateTimer;
        private static int ProductCount;
        private void InitializeProductList()
        {
            ProductCount = 0;
            for (int i = 0; i < NUM_PRODUCTS; i++)
            {
                AddProduct(null);
            }
        }
        private void AddProduct(object state)
        {
            App.Current.Dispatcher.Invoke(() =>
            {
                var product = new Product();
                product.CreateFakeData();
                product.ProductId = ProductCount + 1;
                Products.Add(product);
                ProductCount++;
            });
        }


        public static List<Order> Orders = new List<Order>();
        private Timer OrderUpdateTimer;
        private static long OrderCount;
        private void InitializeOrderList()
        {
            OrderCount = 0;
            for (int i = 0; i < NUM_ORDERS; i++)
            {
                AddOrder(null);
            }
        }
        private void AddOrder(object state)
        {
            App.Current.Dispatcher.Invoke(() =>
            {
                var order = new Order();
                order.CreateFakeData();
                order.OrderId = OrderCount + 1;
                order.ProductId = DataUtils.RandomInt(NUM_PRODUCTS);
                order.UserId = DataUtils.RandomInt(NUM_USERS);
                Orders.Add(order);
                OrderCount++;
            });
        }

    }
}
