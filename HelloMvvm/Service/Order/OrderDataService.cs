﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloMvvm.Model;
using HelloMvvm.Common;
using System.Threading;

namespace HelloMvvm.Service
{
    class OrderDataService : OrderDataServiceBase
    {
      
        public OrderDataService()
        {
            FakeDatabase.Orders.ForEach(OrderList.Add);
        }

        

        public void AddNewOrder(Order order)
        {
            OrderList.Add(order);
        }


        public override ObservableCollection<Order> GetProductOrderList(Product product)
        {
            var order_list = OrderList.Where(x => x.ProductId == product.ProductId).ToList();
            return new ObservableCollection<Order>(order_list);
        }

        public override ObservableCollection<Order> GetUserOrderList(User user)
        {
            var order_list = OrderList.Where(x => x.UserId == user.UserId).ToList();
            return new ObservableCollection<Order>(order_list);
        }
    }
}
