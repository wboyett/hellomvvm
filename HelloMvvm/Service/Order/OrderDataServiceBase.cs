﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloMvvm.Model;

namespace HelloMvvm.Service
{
    abstract class OrderDataServiceBase : IOrderDataService
    {
        public OrderDataServiceBase()
        {
            OrderList = new ObservableCollection<Order>();
        }

        protected ObservableCollection<Order> OrderList;
        public ObservableCollection<Order> GetOrderList()
        {
            return OrderList;
        }

        public abstract ObservableCollection<Order> GetUserOrderList(User user);
        public abstract ObservableCollection<Order> GetProductOrderList(Product product);
    }
}
