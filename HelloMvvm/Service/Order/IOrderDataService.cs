﻿using HelloMvvm.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloMvvm.Service
{
    public interface IOrderDataService
    {
        ObservableCollection<Order> GetOrderList();
        ObservableCollection<Order> GetUserOrderList(User user);
        ObservableCollection<Order> GetProductOrderList(Product product);
    }
}
