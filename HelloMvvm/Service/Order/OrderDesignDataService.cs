﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloMvvm.Model;
using HelloMvvm.Common;

namespace HelloMvvm.Service
{
    class OrderDesignDataService : OrderDataServiceBase
    {
        public OrderDesignDataService()
        {
            for (int i = 0; i < 10; i++)
            {
                var order = new Order();
                order.CreateFakeData();
                OrderList.Add(order);
            }
        }

        public override ObservableCollection<Order> GetProductOrderList(Product product)
        {
            var order_list = new ObservableCollection<Order>();
            var num_orders = DataUtils.RandomInt(1000);
            for (int i = 0; i < num_orders; i++)
            {
                var order = new Order();
                order.CreateFakeData();
                order.ProductId = product.ProductId;
                order_list.Add(order);
            }
            return order_list;
        }

        public override ObservableCollection<Order> GetUserOrderList(User user)
        {
            var order_list = new ObservableCollection<Order>();
            var num_orders = DataUtils.RandomInt(2, 100);
            for (int i = 0; i < num_orders; i++)
            {
                var order = new Order();
                order.CreateFakeData();
                order.UserId = user.UserId;
                order_list.Add(order);
            }
            return order_list;
        }
    }
}
