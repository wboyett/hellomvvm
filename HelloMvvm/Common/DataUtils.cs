﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HelloMvvm.Common
{
    public static class DataUtils
    {
        #region Comaprison Utilities

        static public bool Compare(bool a, bool b)
        {
            return (a == b);
        }

        static public bool Compare(int a, int b)
        {
            return (a == b);
        }

        static public bool Compare(float a, float b)
        {
            return (a == b);
        }

        static public bool Compare(string a, string b)
        {
            return (a == b);
        }

        static public bool Compare(DateTime a, DateTime b)
        {
            // NOTE: db truncates DateTime Ticks resolution (from 100ns) to 1us
            return (Math.Abs((a - b).Ticks) < 11);
        }

        static public bool Compare(TimeSpan a, TimeSpan b)
        {
            // NOTE: db truncates DateTime Ticks resolution (from 100ns) to 1us
            return (Math.Abs((a - b).Ticks) < 11);
        }

        static public bool Compare(float[] a, float[] b)
        {
            if (a.Length != b.Length)
            {
                return false;
            }

            for (int i = 0; i < a.Length; i++)
            {
                if (!Equals(a[i], b[i]))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Random Value Utilities

        static Random rand = new Random();

        static public bool RandomBool()
        {
            return (rand.Next(0, 2) == 1);
        }

        static public int RandomInt()
        {
            return (rand.Next());
        }

        static public int RandomInt(int maxValue)
        {
            return (rand.Next(maxValue));
        }

        static public int RandomInt(int minValue, int maxValue)
        {
            return (rand.Next(minValue, maxValue));
        }

        // force any 0 value to be -1 (Ids can't be zero, -1 signifies null)
        static public int RandomId(int maxValue)
        {
            var id = RandomInt(maxValue);
            id = id > 0 ? id : -1;
            return id;
        }

        static public float RandomFloat()
        {
            return ((float)rand.NextDouble());
        }

        static public float RandomFloat(float maxValue)
        {
            return RandomFloat(0, maxValue);
        }

        static public float RandomFloat(float minValue, float maxValue)
        {
            float scale = (maxValue - minValue);
            return ((float)rand.NextDouble() * scale + minValue);
        }

        static public float[] RandomFloatArray(int length)
        {
            float[] data = new float[length];
            for (int i = 0; i < length; i++)
            {
                data[i] = RandomFloat();
            }
            return data;
        }

        static public float[] RandomFloatArray(int length, float maxValue)
        {
            return RandomFloatArray(length, 0, maxValue);
        }

        static public float[] RandomFloatArray(int length, float minValue, float maxValue)
        {
            return RandomFloatArray(length, minValue, maxValue, false);
        }

        static public float[] RandomFloatArray(int length, float minValue, float maxValue, bool monotonicallIncreasing)
        {
            float[] data = new float[length];
            for (int i = 0; i < length; i++)
            {
                data[i] = RandomFloat(minValue, maxValue);
            }

            if (monotonicallIncreasing)
            {
                var sorted = data.ToList();
                sorted.Sort();
                data = sorted.ToArray<float>();
            }
            return data;
        }


        static public float[] PerlinNoise(int length, float mean, float std)
        {
            double step = 0.01;
            var seed = RandomInt();
            var xvals = Enumerable.Range(seed, length).Select(x => (x * step)); ;
            var yvals = new List<float>();
            foreach (var x in xvals)
            {
                float r = (float)Perlin.noise(x, x, 0);
                yvals.Add(r * std + mean);
            }

            return yvals.ToArray();
        }

        private const string CHAR_LUT = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        static public string RandomString()
        {
            return RandomString(32);
        }

        static public string RandomString(int maxChars)
        {
            return RandomString(1, maxChars);
        }

        static public string RandomString(int minChars, int maxChars)
        {
            var num_chars = rand.Next(minChars, maxChars);
            var result = new string(Enumerable.Repeat(CHAR_LUT, num_chars).Select(s => s[rand.Next(s.Length)]).ToArray());
            return result;
        }

        static public string RandomWords(int maxWords)
        {
            var num_words = rand.Next(1, maxWords);
            var list_len = WordList.words.Count;
            string[] buffer = new string[num_words];
            for (int i = 0; i < num_words; i++)
            {
                buffer[i] = WordList.words[rand.Next(list_len)];
            }

            return string.Join(" ", buffer);
        }

        static public DateTime RandomDateTime()
        {
            int year = RandomInt(1, 9999);
            int month = RandomInt(1, 13);
            int day = RandomInt(1, DateTime.DaysInMonth(year, month));
            return new DateTime(year, month, day, RandomInt(24), RandomInt(60), RandomInt(60));
        }

        static public DateTime RandomDateTime(DateTime minValue, DateTime maxValue)
        {
            TimeSpan range;
            if (maxValue > minValue)
                range = (maxValue - minValue);
            else
                range = (minValue - maxValue);

            DateTime rando = minValue + RandomTimeSpan(range);
            return rando;
        }

        static public TimeSpan RandomTimeSpan()
        {
            int days = RandomInt(TimeSpan.MaxValue.Days - 1);
            return new TimeSpan(days, RandomInt(24), RandomInt(60), RandomInt(60));
        }

        static public TimeSpan RandomTimeSpan(TimeSpan maxValue)
        {
            int maxMinutes = (int)maxValue.TotalMinutes;
            return TimeSpan.FromMinutes((double)RandomInt(maxMinutes));
        }

        static public TimeSpan RandomTimeSpan(TimeSpan minValue, TimeSpan maxValue)
        {
            TimeSpan range;
            if (maxValue > minValue)
                range = (maxValue - minValue);
            else
                range = (minValue - maxValue);

            TimeSpan rando = RandomTimeSpan(range) + minValue;
            return rando;
        }

        static public TimeSpan RandomTimeSpan(int minDays, int maxDays)
        {
            TimeSpan min = new TimeSpan(minDays, RandomInt(24), RandomInt(60), RandomInt(60));
            TimeSpan max = new TimeSpan(maxDays, RandomInt(24), RandomInt(60), RandomInt(60));
            return RandomTimeSpan(min, max);
        }

        #endregion



        static public float interp1(float x0, float x1, float y0, float y1, float x)
        {
            if ((x1 - x0) == 0)
            {
                return (y0 + y1) / 2;
            }
            return y0 + (x - x0) * (y1 - y0) / (x1 - x0);
        }

        static public double Gaussian(double x, double mean, double std, double ampl)
        {
            return ampl * Math.Exp(-(Math.Pow(x - mean, 2)) / (2 * Math.Pow(std, 2)));
        }

        public static double[] Gaussian(int length, double mean, double std, double ampl)
        {
            double[] y = new double[length];
            for (int i = 0; i < length; i++)
            {
                double x = mean - (length / 2) + i;
                y[i] = Gaussian(x, mean, std, ampl);
            }

            return y;
        }

    }


    public static class Perlin
    {
        static public double noise(double x, double y, double z)
        {
            int X = (int)Math.Floor(x) & 255;
            int Y = (int)Math.Floor(y) & 255;
            int Z = (int)Math.Floor(z) & 255;
            x -= Math.Floor(x);
            y -= Math.Floor(y);
            z -= Math.Floor(z);
            double u = fade(x);
            double v = fade(y);
            double w = fade(z);
            int A = p[X] + Y,
                AA = p[A] + Z,
                AB = p[A + 1] + Z,
                B = p[X + 1] + Y,
                BA = p[B] + Z,
                BB = p[B + 1] + Z;

            return lerp(w, lerp(v, lerp(u, grad(p[AA], x, y, z),
                                           grad(p[BA], x - 1, y, z)),
                                   lerp(u, grad(p[AB], x, y - 1, z),
                                           grad(p[BB], x - 1, y - 1, z))),
                           lerp(v, lerp(u, grad(p[AA + 1], x, y, z - 1),
                                           grad(p[BA + 1], x - 1, y, z - 1)),
                                   lerp(u, grad(p[AB + 1], x, y - 1, z - 1),
                                           grad(p[BB + 1], x - 1, y - 1, z - 1))));
        }
        static double fade(double t)
        {
            return t * t * t * (t * (t * 6 - 15) + 10);
        }
        static double lerp(double t, double a, double b)
        {
            return a + t * (b - a);
        }
        static double grad(int hash, double x, double y, double z)
        {
            int h = hash & 15;                      // CONVERT LO 4 BITS OF HASH CODE
            double u = h < 8 ? x : y,                 // INTO 12 GRADIENT DIRECTIONS.
                   v = h < 4 ? y : h == 12 || h == 14 ? x : z;
            return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
        }

        static int[] p = { 151,160,137,91,90,15,
                           131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
                           190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
                           88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
                           77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
                           102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
                           135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
                           5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
                           223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
                           129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
                           251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
                           49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
                           138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180,
                           151,160,137,91,90,15,
                           131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
                           190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
                           88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
                           77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
                           102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
                           135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
                           5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
                           223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
                           129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
                           251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
                           49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
                           138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
                           };

    }

}
