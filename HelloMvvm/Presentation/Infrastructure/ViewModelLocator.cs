/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:CsMvvmSandbox"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using HelloMvvm.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace HelloMvvm.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                // Create design time view services and models
                SimpleIoc.Default.Register<INavigationService, NavigationDesignService>();
                SimpleIoc.Default.Register<IUserDataService, UserDesignDataService>();
                SimpleIoc.Default.Register<IProductDataService, ProductDesignDataService>();
                SimpleIoc.Default.Register<IOrderDataService, OrderDesignDataService>();
            }
            else
            {
                // HACK: this is a lazy hack to avoid actually using a database
                FakeDatabase data = new FakeDatabase();

                // Create run time view services and models
                SimpleIoc.Default.Register<INavigationService, NavigationService>();
                SimpleIoc.Default.Register<IUserDataService, UserDataService>();
                SimpleIoc.Default.Register<IProductDataService, ProductDataService>();
                SimpleIoc.Default.Register<IOrderDataService, OrderDataService>();
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<UsersViewModel>();
            SimpleIoc.Default.Register<ProductsViewModel>();
            SimpleIoc.Default.Register<OrdersViewModel>();
        }

        public MainViewModel MainView
        {
            get { return ServiceLocator.Current.GetInstance<MainViewModel>(); }
        }

        public UsersViewModel UsersView
        {
            get { return ServiceLocator.Current.GetInstance<UsersViewModel>(); }
        }

        public ProductsViewModel ProductsView
        {
            get { return ServiceLocator.Current.GetInstance<ProductsViewModel>(); }
        }

        public OrdersViewModel OrdersView
        {
            get { return ServiceLocator.Current.GetInstance<OrdersViewModel>(); }
        }

        public ReportsViewModel ReportsView
        {
            get { return ServiceLocator.Current.GetInstance<ReportsViewModel>(); }
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}