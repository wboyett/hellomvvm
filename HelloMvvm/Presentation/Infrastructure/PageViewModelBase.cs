using HelloMvvm.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using HelloMvvm.Model;

namespace HelloMvvm.ViewModel
{
    /// <summary>
    ///
    /// </summary>
    public class PageViewModelBase : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public PageViewModelBase()
        {
            NavService = ServiceLocator.Current.GetInstance<INavigationService>();


        }

        private string _PageName;
        public string PageName
        {
            get { return _PageName; }
            set { Set(ref _PageName, value, nameof(PageName)); }
        }


        #region Page navigation methods

        INavigationService NavService;

        private bool _ShowingUsersPage;
        public bool ShowingUsersPage
        {
            get { return _ShowingUsersPage; }
            set { Set(ref _ShowingUsersPage, value, nameof(ShowingUsersPage)); }
        }
        public void ShowUserPage(User user)
        {
            NavService.GotoUserPage(user);
        }
        private RelayCommand<object> _ShowUserCommand;
        public ICommand ShowUserCommand
        {
            get { return SetRelayCommand(ref _ShowUserCommand, (param) => ShowUserPage((User)param)); }
        }


        private bool _ShowingProductsPage;
        public bool ShowingProductsPage
        {
            get { return _ShowingProductsPage; }
            set { Set(ref _ShowingProductsPage, value, nameof(ShowingProductsPage)); }
        }
        public void ShowProductPage(Product product)
        {
            NavService.GotoProductPage(product);
        }
        private RelayCommand<object> _ShowProductCommand;
        public ICommand ShowProductCommand
        {
            get { return SetRelayCommand(ref _ShowProductCommand, param => ShowProductPage((Product)param)); }
        }


        private bool _ShowingOrdersPage;
        public bool ShowingOrdersPage
        {
            get { return _ShowingOrdersPage; }
            set { Set(ref _ShowingOrdersPage, value, nameof(ShowingOrdersPage)); }
        }
        public void ShowOrderPage(Order order)
        {
            NavService.GotoOrderPage(order);
        }
        private RelayCommand<object> _ShowOrderCommand;
        public ICommand ShowOrderCommand
        {
            get { return SetRelayCommand(ref _ShowOrderCommand, param => ShowOrderPage((Order)param)); }
        }


        private bool _ShowingReportsPage;
        public bool ShowingReportsPage
        {
            get { return _ShowingReportsPage; }
            set { Set(ref _ShowingReportsPage, value, nameof(ShowingReportsPage)); }
        }
        public void ShowReportPage()
        {
            NavService.GotoReportPage();
        }
        private RelayCommand<object> _ShowReportCommand;
        public ICommand ShowReportCommand
        {
            get { return SetRelayCommand(ref _ShowReportCommand, param => ShowReportPage()); }
        }

        #endregion


        #region RelayCommand helpers

        protected virtual RelayCommand<object> SetRelayCommand(ref RelayCommand<object> command, Action<object> action, Func<object, bool> predicate)
        {
            if (command == null)
            {
                command = new RelayCommand<object>(action, predicate);
            }
            return command;
        }

        protected virtual RelayCommand<object> SetRelayCommand(ref RelayCommand<object> command, Action<object> action)
        {
            if (command == null)
            {
                command = new RelayCommand<object>(action);
            }
            return command;
        }

        protected virtual RelayCommand SetRelayCommand(ref RelayCommand command, Action action, Func<bool> predicate)
        {
            if (command == null)
            {
                command = new RelayCommand(action, predicate);
            }
            return command;
        }

        protected virtual RelayCommand SetRelayCommand(ref RelayCommand command, Action action)
        {
            if (command == null)
            {
                command = new RelayCommand(action);
            }
            return command;
        }

        #endregion

    }
}