using HelloMvvm.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System.Windows.Input;
using HelloMvvm.Model;

namespace HelloMvvm.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : PageViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            locator = new ViewModelLocator();

            Messenger.Default.Register<PageNav>(this, page => LoadPage(page));

            ShowProductsPage(null);
        }

        ViewModelLocator locator;

        private PageViewModelBase _MainPage;
        public PageViewModelBase MainPage
        {
            get { return _MainPage; }
            set { Set(ref _MainPage, value, nameof(MainPage)); }
        }


        #region Main Page methods

        private void LoadPage(PageNav page)
        {
            switch ((Page)page.Page)
            {
                case Page.Users:
                    ShowUsersPage((User)page.SelectedItem);
                    break;
                case Page.Products:
                    ShowProductsPage((Product)page.SelectedItem);
                    break;
                case Page.Orders:
                    ShowOrdersPage((Order)page.SelectedItem);
                    break;
                case Page.Reports:
                    ShowReportsPage();
                    break;
            }
        }

        private void ShowUsersPage(User user)
        {
            if (user != null)
            {
                locator.UsersView.SelectedUser = user;
            }
            MainPage = locator.UsersView;
            ShowingUsersPage = true;
        }

        private void ShowProductsPage(Product product)
        {
            if (product != null)
            {
                //locator.ProductsView.SelectedProduct = product;
            }
            MainPage = locator.ProductsView;
            ShowingProductsPage = true;
        }

        private void ShowOrdersPage(Order order)
        {
            if (order != null)
            {
                locator.OrdersView.SelectedOrder = order;
            }
            MainPage = locator.OrdersView;
            ShowingOrdersPage = true;
        }

        private void ShowReportsPage()
        {
            MainPage = locator.ReportsView;
            ShowingReportsPage = true;
        }

        #endregion

    }
}