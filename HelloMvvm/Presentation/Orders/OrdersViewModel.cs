using HelloMvvm.Service;
using GalaSoft.MvvmLight;
using Microsoft.Practices.ServiceLocation;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.CommandWpf;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;
using HelloMvvm.Model;

namespace HelloMvvm.ViewModel
{
    /// <summary>
    ///
    /// </summary>
    public class OrdersViewModel : PageViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public OrdersViewModel(IOrderDataService orderService)
        {
            PageName = "Orders";

            OrderAccess = orderService;
            OrderList = OrderAccess.GetOrderList();

            this.PropertyChanged += ViewModelPropertyChanged;

            SelectedOrder = OrderList[0];
        }

        private void ViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedOrder)) { }
        }


        private IOrderDataService OrderAccess;
        public ObservableCollection<Order> OrderList { get; private set; }

        private Order _SelectedOrder;
        public Order SelectedOrder
        {
            get { return _SelectedOrder; }
            set { Set(ref _SelectedOrder, value, nameof(SelectedOrder)); }
        }


    }
}