using HelloMvvm.Service;
using GalaSoft.MvvmLight;
using Microsoft.Practices.ServiceLocation;
using HelloMvvm.Model;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace HelloMvvm.ViewModel
{
    /// <summary>
    ///
    /// </summary>
    public class ProductsViewModel : PageViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public ProductsViewModel(IProductDataService productService)
        {
            PageName = "Products";

            ProductAccess = productService;
            ProductList = ProductAccess.GetProductList();

            this.PropertyChanged += ViewModelPropertyChanged;

            SelectedProduct = ProductList[0];
        }

        private void ViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedProduct)) { }
        }


        private IProductDataService ProductAccess;
        public ObservableCollection<Product> ProductList { get; private set; }

        private Product _SelectedProduct;
        public Product SelectedProduct
        {
            get { return _SelectedProduct; }
            set { Set(ref _SelectedProduct, value, nameof(SelectedProduct)); }
        }

    }
}