using HelloMvvm.Model;
using HelloMvvm.Service;
using GalaSoft.MvvmLight;
using Microsoft.Practices.ServiceLocation;
using System.Collections.ObjectModel;
using System;
using System.ComponentModel;

namespace HelloMvvm.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class UsersViewModel : PageViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public UsersViewModel(IUserDataService userService, IOrderDataService orderService)
        {
            PageName = "Users";

            UserAccess = userService;
            OrderAccess = orderService;

            UserList = UserAccess.GetUserList();

            this.PropertyChanged += ViewModelPropertyChanged;

            SelectedUser = UserList[0];
        }

        private void ViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedUser)) { UpdateSelectedUser(); }
        }

        

        private IUserDataService UserAccess;
        private IOrderDataService OrderAccess;


        public ObservableCollection<User> UserList { get; private set; }

        private User _SelectedUser;
        public User SelectedUser
        {
            get { return _SelectedUser; }
            set { Set(ref _SelectedUser, value, nameof(SelectedUser)); }
        }

        private void UpdateSelectedUser()
        {
            SelectedUserOrderHistory = OrderAccess.GetUserOrderList(SelectedUser);
            RaisePropertyChanged(nameof(SelectedUserOrderHistory));
        }


        public ObservableCollection<Order> SelectedUserOrderHistory { get; private set; }

    }
}